# ReCoNe - Reconnect Network Drives

(c) 2018 [M. Busche](mailto:moin@butenostfreesen.de)

https://blog.butenostfreesen.de/

## HTML view of this document

[View latest version online](https://gitlab.com/m-busche/recone/blob/master/README.md)

## License

See LICENSE.txt.

## Purpose

ReCoNe reconnects Windows network drives by simply opening an Explorer window for each (disconnected) network drive after the user logs on. All opened windows will be magically closed afterwards.

## Files

| File                     | Purpose                                              |
| ------------------------ | ---------------------------------------------------- |
| ReCoNe.exe               | Core program, opens and closes explorer windows      |
| SaveConnectedDrives.exe  | Saves currently connected network drives             |
| ReCoNe-GUI.exe           | Configures ReCoNe Windows Task                       |
| README.MD                | Useage and license                                   |
| src\\\*.au3, src\\\*.ico | AutoIT source files and icon(s)                      |

## Usage

Three steps to connect your network drives at logon:

- Make sure that all your network drives are connected (no red cross in explorer)
- Start `SaveConnectedDrives.exe` to save the list of drive letters (to `%AppData%\networkdrives.txt`)
- Start `ReCoNe-GUI.exe` to add a scheduled task to Windows Task Manager (or manually add a task starting `ReCoNe.exe` after user logon). `ReCoNe-GUI.exe` requires admin rights to handle the Windows Task Manager.

## Drive connections changed

Just start `SaveConnectedDrives.exe` again to save an updated list of your connected drive letters.

## Uninstall

- Remove the ReCoNe task from Windows Task Manager - either manually or by using the *Delete* button from `ReCoNe-GUI.exe`.
- Setup version: Uninstall the software by using the Windows Program Manager
- Portable version from Zip file: Delete all files and the folder you extracted the Zip file to.

## Updates

If there will be an update it will be published [here](https://blog.butenostfreesen.de/2018/10/19/Windows-Netzlaufwerke-wiederherstellen/#Download).

## AutoIT source code

The source code is published at [Gitlab](https://gitlab.com/m-busche/recone), it can be compiled with [AutoIT](https://www.autoitscript.com/site/).

## 3rd party licenses

The ReCoNe icon was taken from [Open Icon Library](https://sourceforge.net/projects/openiconlibrary/).

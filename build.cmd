@echo off
set version=1.1
del /q release\*
mkdir bin
del /q bin\*.exe

if "%home%"=="1" (
    set compiler="C:\Program Files (x86)\AutoIt3\Aut2Exe\Aut2exe.exe"
    set zip="D:\Program Files\7-Zip\7z.exe"
    set inno="D:\Program Files (x86)\Inno Setup 5\Compil32.exe"
) else (
    set compiler="C:\Program Files (x86)\AutoIt3\Aut2Exe\Aut2exe.exe"
    set zip="C:\Program Files\7-Zip\7z.exe"
    set inno="C:\Program Files (x86)\Inno Setup 5\Compil32.exe"
)

echo Aut2Exe: %compiler%
echo 7z.exe: %zip%
echo Compil32.exe: %inno%

cd src
%compiler% /in ReCoNe-GUI.au3
%compiler% /in ReCoNe.au3
%compiler% /in SaveConnectedDrives.au3
%inno% /cc innosetup.iss

cd ..
%zip% a release\ReCoNe_%version%.zip @listfile.txt
move release\setup_ReCoNe.exe release\setup_ReCoNe_%version%.exe

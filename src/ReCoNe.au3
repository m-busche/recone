; Recover formerly connected network drives after the Windows start or logon
; (c) 2018 M. Busche, markus@butenostfreesen.de
;
; License: CC-BY 4.0 (https://creativecommons.org/licenses/by/4.0/)
;
;
; Usage:
; (1)  Make sure that all your network drives are connected
; (2)  Start 'SaveConnectedDrivesList.exe' - all connected drive letters will be saved to a text file (%AppData%\networkdrives.txt)
; (3)  Add ReCoNe.exe to your Autostart group (or add a Scheduled Task) to let it start after each logon.
; (3a) Alternatively run 'AddScheduledTask.exe' once after step (2)
;
; You can change the list of network drives by editing of %AppData%\networkdrives.txt
;
#pragma compile(Out, ..\bin\ReCoNe.exe)
#pragma compile(LegalCopyright, "© Markus Busche moin@butenostfreesen.de")
#pragma compile(Icon, network-connect-3.ico)

#include <AutoItConstants.au3>
#include <MsgBoxConstants.au3>
#include <File.au3>

Opt("WinTitleMatchMode", 2)

Local $sSettingsfile = @AppDataDir & "\networkdrives.txt"
If not FileExists($sSettingsfile) Then
	MsgBox($MB_SYSTEMMODAL, "ReCoNe", "Warning. the file containing the list of your network drives does not exist. Run 'SaveConnectedDrivesList.exe' first!" & @CRLF & @CRLF & "[This message closes after 10 seconds]", 10)
	Exit 1
EndIf

; Read formerly connected drives from file
Local $aNetworkdrives = DriveGetDrive($DT_NETWORK)
_FileReadToArray($sSettingsfile, $aNetworkdrives)

$iNumDrives = UBound($aNetworkdrives) -1
if $iNumDrives > 0 Then
	; Open explorer.exe on every network drive
	For $i = 1 to UBound($aNetworkdrives) -1
		Run("explorer.exe " & StringUpper($aNetworkdrives[$i]))
		; Wait for the drive letter in Explorer windows title
		WinWait(StringUpper($aNetworkdrives[$i]), "", 30)
		; Close the window
		WinClose(StringUpper($aNetworkdrives[$i]), "")
	Next
	MsgBox($MB_SYSTEMMODAL, "ReCoNe", $iNumDrives & " network drives have been recovered"  & @CRLF & "[This message closes after 2 seconds]", 2)
	Exit 0
Else
	MsgBox($MB_SYSTEMMODAL, "ReCoNe", "Warning, the settings file seems to be empty. Run 'SaveConnectedDrivesList.exe' to save your connected network drives." & @CRLF & @CRLF & "[This message closes after 10 seconds]", 10)
	Exit 
EndIf

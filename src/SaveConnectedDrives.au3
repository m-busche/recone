#pragma compile(Out, ..\bin\SaveConnectedDrives.exe)
#pragma compile(LegalCopyright, "© Markus Busche moin@butenostfreesen.de")
#pragma compile(Icon, network-connect-3.ico)


#include <AutoItConstants.au3>
#include <MsgBoxConstants.au3>
#include <ButtonConstants.au3>
#include <File.au3>

Local $sSettingsfile = @AppDataDir & "\networkdrives.txt"

$mAnswer = MsgBox (4, "ReCoNe" ,"Are all network drives connected at this moment?")
If $mAnswer = 6 Then
    Local $aNetworkdrives = DriveGetDrive($DT_NETWORK)

    _ArrayDelete($aNetworkdrives, 0)
    Local $iNumDrives = UBound($aNetworkdrives)
    _FileWriteFromArray ($sSettingsfile , $aNetworkdrives)

    MsgBox($MB_SYSTEMMODAL, "ReCoNe", $iNumDrives & " connected network drives saved." & @CRLF & @CRLF  & "Restart me if the list of network drives has changed." & @CRLF & @CRLF  & "[This message closes after 5 seconds]", 5)
Else
    MsgBox($MB_SYSTEMMODAL, "ReCoNe", "Start again when all drives are connected or if the list of network drives has changed."& @CRLF & @CRLF  & "[This message closes after 5 seconds]", 5)
EndIf

#requireadmin
#pragma compile(Out, ..\bin\ReCoNe-GUI.exe)
#pragma compile(LegalCopyright, "© Markus Busche moin@butenostfreesen.de")
#pragma compile(Icon, network-connect-3.ico)


#include <AutoItConstants.au3>
#include <MsgBoxConstants.au3>
#include <ButtonConstants.au3>
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#include <File.au3>

Local $sVersion = "1.1"
Local $aFile = @ScriptDir & '\ReCoNe.exe'
Local $sSettingsfile = @AppDataDir & "\networkdrives.txt"

If not FileExists($sSettingsfile) Then
	MsgBox($MB_SYSTEMMODAL, "ReCoNe", "Warning, the settings file containing the list of your network drives does not exist. Run 'SaveConnectedDrivesList.exe' first!" & @CRLF & @CRLF & "[This message closes after 10 seconds]", 10)
	Exit 1
EndIf

#Region ### Start GUI section ###
$Form1 = GUICreate("ReCoNe-GUI", 244, 190, 192, 124)
$bInstall = GUICtrlCreateButton("Create or update ReCoNe task", 16, 8, 211, 25)
$bDelete = GUICtrlCreateButton("Delete ReCoNe task", 16, 42, 211, 25)
$bShow = GUICtrlCreateButton("Open Windows Task Scheduler", 16, 76, 211, 25)
$bAbout = GUICtrlCreateButton("About ReCoNe", 16, 110, 211, 25)
$bExit = GUICtrlCreateButton("Exit", 16, 144, 211, 25)
GUISetState(@SW_SHOW)
#EndRegion ### EndRe GUI section ###

While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			Exit 0
		Case $bInstall
			AddScheduledTask()
		Case $bDelete
			DeleteScheduledTask()
		Case $bShow
			ShowTaskScheduler()
		Case $bAbout
			Aboutbox()
		Case $bExit
			Exit 0
	EndSwitch
	Sleep(10)
WEnd

Func AddScheduledTask()
	If FileExists($aFile) Then
		$sDelay = InputBox("Add Scheduled Task", "This will add a scheduled Task to Windows Task Manager (taskschd.msc). Please enter a delay in seconds [0-60] to wait after login?", "30")
		$sCommand = 'schtasks.exe /Create /SC ONLOGON /DELAY 0000:' & $sDelay & ' /TN "Reconnect Network Drives" /TR ' & _
				'"' & "'" & $aFile & "'" & '"'
		; MsgBox($MB_SYSTEMMODAL, "", $sCommand)
		Run($sCommand, '', @SW_HIDE)
		MsgBox($MB_SYSTEMMODAL, "ReCoNe", "A new task was added (or an existing updated)" & @CRLF & @CRLF & "[This message closes after 5 seconds]", 5)
	Else
		MsgBox($MB_SYSTEMMODAL, "ReCoNe", "Warning, " & $aFile & " was not found! Make sure you run this script from the same folder as ReCoNe.exe." & @CRLF & @CRLF & "[This message closes after 10 seconds]", 10)
		Exit 3
	EndIf
EndFunc 

Func DeleteScheduledTask()
	$sCommand = 'schtasks.exe /Delete /F /TN "\Reconnect Network Drives"'
	Run($sCommand, '', @SW_HIDE)
	If not @error Then
		MsgBox($MB_SYSTEMMODAL, "ReCoNe", "Scheduled task deleted from Windows Task Manager." & @CRLF & @CRLF & "[This message closes after 5 seconds]", 5)
	Else
		MsgBox($MB_SYSTEMMODAL, "ReCoNe", "Failed deleting task." & @CRLF & @CRLF & "[This message closes after 5 seconds]", 5)
	EndIf
EndFunc

Func ShowTaskScheduler()
	Run(@ComSpec & ' /c ' & 'taskschd.msc /s', '', @SW_HIDE)
EndFunc

Func Aboutbox()
	MsgBox($MB_SYSTEMMODAL, "About ReCoNe", "ReCoNe version " & $sVersion & @CRLF & @CRLF & "© 2018 M. Busche" & @CRLF & "moin@exilostfriesen.de" & _
						@CRLF & @CRLF & "Usage: See README.MD" & @CRLF & _
						"License: See LICENSE.TXT")
EndFunc
